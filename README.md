# Grape

Grape is a simple and minimalist [Jekyll](https://jekyllrb.com) theme focused on blogging.

## Info

The theme have a minimal and prette (I guess) CSS for the most common elements.


## Usage

Grape have common configurations: 

Configure your ´_config,yml´

## General options

- lang:     	Language of the site
- author:     Author of the posts
- title:          Site title
- subtitle:   Tagline
- description:  Description of the site for SEO
- logo:          Image for SEO in OpenGraph 



## Theme options

The theme have configured in the `_config.yml` file a default options for the new pages/posts.

- layout: "post "
- author: "Admin"
- paginate: 6 _default number of posts per page_
- permalink: "/:title/" _default permalink per post_
- paginate_path: "/page/:num" _default format for paginate url_

## Post/Page options

- image:   _Define a post/page header image, must be and absolute path (including the baseurl).

## Data options

### Date language

The theme include a basic translation for dates (in blog layout and posts), you must edit the file `_data/months.yml` and replace **name**  value with the month in your language. 

### Navbar links

In `_data/navbar.yml` you can configure the main page header links with the format **name** for the link title and **link** for the link url without include the baseurl.

### Footer links

For the footer the file is `_data/navbar_footer.yml` the format is similar to the main navbar **name** for the link name. **link** is a full url for social media  networks and **icon** is the main name for the icon to show. based in [Fontawesome](https://fontawesome.com/), example: the full icon name for Facebook is "fab fas-facebook", you must entry only _facebook_

## TODO

- Background customization
- Color customization
- More language options (Next and Previous pagination links)
- Add search form and integrate lunarjs for search
- Add css forms

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/Max131/grape. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## Development

To set up your environment to develop this theme, run `bundle install`.

Your theme is setup just like a normal Jekyll site! To test your theme, run `bundle exec jekyll serve` and open your browser at `http://localhost:4000`. This starts a Jekyll server using your theme. Add pages, documents, data, etc. like normal to test your theme's contents. As you make modifications to your theme and to your content, your site will regenerate and you should see the changes in the browser after a refresh, just like normal.

When your theme is released, only the files in `_layouts`, `_includes`, `_sass` and `assets` tracked with Git will be bundled.
To add a custom directory to your theme-gem, please edit the regexp in `grape2.gemspec` accordingly.

## License

The theme is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

