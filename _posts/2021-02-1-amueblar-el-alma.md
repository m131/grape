---
layout: post
title: Amueblar el alma
image: https://picsum.photos/640/200
author: Mario
categories: [poema]
---
Hace tanto tiempo que la casa de mi alma está vacía, sin nadie que habite en ella más que los ecos de mi soledad. Y resulta que misteriosamente coincidimos, y veo que los muebles de tu ser bien podrían caber perfectamente en esta casa. Quizá si tú quieres, podríamos comenzar iluminando todo con tus grandes ojos, que dan una luz tan intensa que iluminaría hasta el último rincón, y un poco más allá en una inmensa pecera pondría los corales de tu boca, para que con sus formas y colores alegren el día.

Para contrarrestar el eco de mi solitaria voz, le vendría muy bien el sonido estéreo de tu risa, instalado muy cerca de mis oídos, para escucharla como si fueran los pájaros que cantan al amanecer.

Claro que para descansar, nada mejor que la almohada de tus piernas, donde podría reposar mi cabeza y dormir profundamente la siesta, cobijado por el denso manto cósmico de tu oscura cabellera; tu mirada tranquila bien podría ser los postigos de las ventanas, que cuidarían de que entre la luz adecuada para siempre vernos de frente.

Pero además, te pediría traer todas tus pertenecías y nada a medias, por eso, reservaría una habitación completa para todo el equipaje que cargas, y lo pondríamos ahí, con cuidado, para que con tiempo y paciencia acomodarlo poco a poco, que al final algún lugar le encontraremos donde quepa perfectamente, porque hasta el último detalle dentro de ese equipaje, es parte de lo que ahora eres, y la casa de mi alma solo puede albergar a alguien completa, sin pretextos, sólo en tu totalidad. Así que, la puerta está abierta e invitándote para que comiences a amueblar mi alma. 