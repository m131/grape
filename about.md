---

layout: page
title: About me
---
# Acerca de mi

![Me](https://picsum.photos/seed/z/200){:style="float:right; margin: 0.5rem"}
Ad _aliquip_ do sed id ut dolore anim pariatur in nulla ea anim dolor aliqua amet ut magna incididunt commodo quis ex est excepteur aute excepteur ut cillum reprehenderit anim reprehenderit eu irure consectetur duis magna commodo sint in incididunt **voluptate** cupidatat culpa[^1] elit fugiat exercitation minim reprehenderit consequat est cillum non in esse consectetur est proident officia laboris laborum proident dolor duis qui voluptate commodo in fugiat officia cupidatat consequat in ut ut pariatur laboris tempor cillum excepteur aute officia dolore adipisicing consectetur consectetur commodo in aliquip minim dolore in officia dolore magna proident proident magna laboris adipisicing enim esse ut dolore ut nulla enim ex in enim consectetur ea quis exercitation eiusmod sunt sunt consequat culpa laboris adipisicing quis cupidatat sunt enim enim qui reprehenderit ut ex officia elit aliqua labore non non sint dolor in laboris fugiat commodo nulla dolor occaecat anim sint laboris laborum duis elit adipisicing `dolore`.

## Two

### Three

#### Four

##### Five

###### Six

| One  | Two  | Three |Four|Five|
| :--- | :--: | ----: |----|----|
| 1    | 2    | 3     | 1  | 2  |
| 4    | 5    | 6     | 4  | 5  |
| 7    | 8    | 9     | 7  | 8  |
| 10   | 11   | 12    |
| 13   | 14   | 15    |
| 16   | 17   | 18    |
| 19   | 20   | 21    | 19 | 20 |

```javascript
class Dog{
    constructor(race, size){
        this.race = race;
        this.size = size;
    }
    bark(){
        console.log('Bark bark...');
    }
}
```

> Lorem ipsum  
> dolor sit amet  
>
>  – Loremio

- One

- Two

- Three

  - Four

  - Five

- Six


![Large image](https://picsum.photos/900/600)


***

  1. One
  2. Two
  3. Thre
     1. Four
     2. Five

Kramdown

: A markdown-superset converter

MMD

: Multimarkdown Abreviation				

H~2~O

X^2^

The main language of the web is HTML.

*[HTML]: Hyper Text Markup Language

***

[^1]: Lorem ipsum dolor sit amet.